using { temporal, cuid } from '@sap/cds/common';
namespace de.conet.cds.test.temporal;

entity Entities : temporal, cuid {
  property  :  String;
}