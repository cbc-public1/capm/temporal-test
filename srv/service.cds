using { de.conet.cds.test.temporal as my } from '../db/schema';

service Service @(path:'/service') {
  entity Entities as projection on my.Entities;
}